#require 'rubygems'
require 'sinatra'
require 'selenium-webdriver'
require 'json'
# require 'pry'
require 'net/http'
require 'uri'
# require 'dotenv'

class DroppedMemberBot < Sinatra::Base

  post '/' do
    # pull ZP Membership ID from params
    zen_planner_membership_id = params['Zen Planner Membership ID']

    #zen_planner_membership_id = "BB6045E3-5A98-45D8-9A02-49EF15E8F7BC"

    zen_planner_membership_url = "https://studio.zenplanner.com/zenplanner/studio/index.html#/main/iframe/zenplanner/studio/person/membership.cfm?membershipId=#{zen_planner_membership_id}"

    #pull Zen Planner password from ENV
    # Dotenv.load
    zen_planner_password = ENV["ZP_PASS"]

    # logging
    # File.write("log.txt","\n" +  zen_planner_password, mode: "a")

    #puts zen_planner_password

    # configure the driver to run in headless mode
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options

    # navigate driver to Zen Planner and log in
    driver.navigate.to "https://studio.zenplanner.com/zenplanner/studio/index.html#/login"
    element = nil

    wait = Selenium::WebDriver::Wait.new(timeout: 10) # seconds
    wait.until { element = driver.find_element(name: "email") }

    element.send_keys "todd@southloopsc.com"

    driver.find_element(id: "idPassword").send_keys zen_planner_password

    # binding.pry

    button = driver.find_element(css: "button").click

    # make sure the page loads
    wait.until { element = driver.find_element(id: "js-search-tab") }

    # navigate to membership
    driver.navigate.to zen_planner_membership_url

    # switch to main iframe
    driver.switch_to.frame("idTheIframe")

    # get the membership type
    wait.until { element = driver.find_element(:xpath, '//*[@id="id_1"]/div/div[1]/table/tbody/tr[1]/td[2]')}
    membership_type = element.text

    # make sure the page loads and hover on drop menu
    wait.until { element = driver.find_element(:xpath, '//*[@id="id_1"]/div/h2/a')}
    driver.action.move_to(element).perform
    sleep 1

    # click on menu to find drop reason
    #elements = driver.find_elements(:xpath, '//*[@id="membershipSelect"]/a[3]')
    element = driver.find_element(:partial_link_text, 'View Drop')
    #element = elements.first
    driver.action.move_to(element).perform
    wait.until { element.displayed? }
    element.click

    # get drop reason from pop-up menu
    wait.until { element = driver.find_element(:id, 'idDropReasonId')}
    selected_option = Selenium::WebDriver::Support::Select.new(element).selected_options
    drop_reason = selected_option.first.text

    driver.quit

    # print drop_reason

    # send a post request to Zapier with email, drop reason, and membership type
    url = URI("https://hooks.zapier.com/hooks/catch/1093047/omg3tc1/")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    request["cache-control"] = 'no-cache'
    request.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"zen_planner_membership_id\"\r\n\r\n#{zen_planner_membership_id}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"drop_reason\"\r\n\r\n#{drop_reason}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"membership_type\"\r\n\r\n#{membership_type}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

    response = http.request(request)
  end

  get '/' do
    "so this shows the dropped-membership url"
  end

end
